<?php

/**
 * Created by PhpStorm.
 * User: huyi
 * Date: 2018/1/22
 * Time: 11:41
 */
class a{
    //定位
    public function position(){

        $ip = $this->GetIp();
        $a  = $this->GetIpxy('218.64.55.198');
        //$latlon = $a['xy'];
        //$url = "http://api.map.baidu.com/geocoder/v2/?ak=C93b5178d7a8ebdb830b9b557abce78b&callback=renderReverse&location=".$latlon."&output=json&pois=0";
        $url = 'http://maps.google.cn/maps/api/geocode/json?latlng='.$latlon.'&language=CN';
        //echo "<a href='{$url}'>aa</a>";die;
        echo json_encode($a);
    }

    //定位到街道地址d
    public function GetIpxy($ip){
        $content = @file_get_contents("http://api.map.baidu.com/location/ip?ak=Gvg7MZ5VYnmZOHW09muMxgXb&ip={$ip}&coor=bd09ll");
        //$content         = @file_get_contents("http://api.map.baidu.com/location/ip?ak=40GXLRoBSegdclkcR7jx0opT34L1tHhs&ip={$ip}&coor=bd09ll");
        $json            = json_decode($content);
        $info            = array();
        $info['xy']      = $json->{'content'}->{'point'}->{'x'} . ',' . $json->{'content'}->{'point'}->{'y'};
        $info['x'] = $json->{'content'}->{'point'}->{'x'};
        $info['y'] = $json->{'content'}->{'point'}->{'y'};
        $info['address'] = $json->{'content'}->{'address'};
        return $info;
    }

    /**
     * @return string
     * 获取真实的客户IP地址
     */
    public function GetIp(){
        $realip  = '';
        $unknown = 'unknown';
        if(isset($_SERVER)){
           // print_r($_SERVER);die;
            if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !empty($_SERVER['HTTP_X_FORWARDED_FOR']) && strcasecmp($_SERVER['HTTP_X_FORWARDED_FOR'], $unknown)){
                $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                foreach($arr as $ip){
                    $ip = trim($ip);
                    if($ip != 'unknown'){
                        $realip = $ip;
                        break;
                    }
                }
            }else{
                if(isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP']) && strcasecmp($_SERVER['HTTP_CLIENT_IP'], $unknown)){
                    $realip = $_SERVER['HTTP_CLIENT_IP'];
                }else{
                    if(isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR']) && strcasecmp($_SERVER['REMOTE_ADDR'], $unknown)){
                        $realip = $_SERVER['REMOTE_ADDR'];
                    }else{
                        $realip = $unknown;
                    }
                }
            }
        }else{
            if(getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), $unknown)){
                $realip = getenv("HTTP_X_FORWARDED_FOR");
            }else{
                if(getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), $unknown)){
                    $realip = getenv("HTTP_CLIENT_IP");
                }else{
                    if(getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), $unknown)){
                        $realip = getenv("REMOTE_ADDR");
                    }else{
                        $realip = $unknown;
                    }
                }
            }
        }
        $realip = preg_match("/[\d\.]{7,15}/", $realip, $matches) ? $matches[0] : $unknown;
        return $realip;
    }
}

$m = new a();
$m->position();